package actors;

public enum ActorsName {

    /**
     * Main actor name.
     */
    MAIN,
    /**
     * Gui manager actor name.
     */
    GUI,
    /**
     * File manager actor name.
     */
    FILE;

    @Override
    public String toString() {
        if (this == ActorsName.MAIN) {
            return "main-actor";
        } else if (this == ActorsName.GUI) {
            return "gui-actor";
        } else if (this == ActorsName.FILE) {
            return "file-manager-actor";
        }
        throw new IllegalCallerException();
    }
}
