package actors.documentActor;

import actors.documentActor.messages.BeginComputationMessage;
import actors.documentActor.messages.ComputeWordMessage;
import actors.fileManagerActor.messages.FinishedComputationMessage;
import akka.actor.AbstractActor;
import model.DocumentContent;
import model.IgnoredWords;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.encryption.AccessPermission;
import org.apache.pdfbox.text.PDFTextStripper;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class DocumentActor extends AbstractActor {

    private final String filePath;
    private final IgnoredWords ignoredWords;
    private final Map<String, Integer> localWordsCount;
    private List<String> documentWordList;

    public DocumentActor(final IgnoredWords ignoredWords, final String filePath) {
        this.filePath = filePath;
        this.ignoredWords = ignoredWords;
        this.localWordsCount = new HashMap<>();
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(BeginComputationMessage.class, msg -> computeDocument())
                .match(ComputeWordMessage.class, msg -> {
                    //on self sent compute word message check if all words are computed
                    if (msg.getIndex() < 0) {
                        //if all words computed, provide file manager actor with my computation result
                        getContext().getParent().tell(new FinishedComputationMessage(localWordsCount), getSelf());
                        //stopping myself after fulfilling my duty
                        getContext().stop(getSelf());
                    } else {
                        //getting the word to compute using index received in the message
                        String word = documentWordList.get(msg.getIndex());
                        //computing the word
                        addToLocalWordsCount(word);
                        //recursively send a message to myself changing the index of the word I have to count
                        getSelf().tell(new ComputeWordMessage((msg.getIndex() - 1)), getSelf());
                    }
                }).build();
    }

    private void addToLocalWordsCount(final String string) {
        if (!ignoredWords.getWords().contains(string)) {
            if (localWordsCount.containsKey(string)) {
                localWordsCount.replace(string, localWordsCount.get(string) + 1);
            } else {
                localWordsCount.put(string, 1);
            }
        }
    }

    private void computeDocument() {
        //trying to load document
        try {
            File fileToParse = new File(filePath);
            PDDocument document = PDDocument.load(fileToParse);
            AccessPermission ap = document.getCurrentAccessPermission();
            if (ap.canExtractContent()) {
                PDFTextStripper stripper = new PDFTextStripper();
                stripper.setSortByPosition(false);
                stripper.setStartPage(1);
                stripper.setEndPage(document.getNumberOfPages());
                String documentContent = new String(stripper.getText(document).trim().getBytes(), StandardCharsets.UTF_8);
                document.close();
                //creating a list containing the words to be computed
                documentWordList = DocumentContent.createInstance(documentContent).getWords();
                //send the first message to myself to start the recursion after loading the document
                getSelf().tell(new ComputeWordMessage(documentWordList.size() - 1), getSelf());
            } else {
                System.out.println(fileToParse.getName() + " skipped, you do not have permission to extract text");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
