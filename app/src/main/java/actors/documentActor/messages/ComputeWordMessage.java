package actors.documentActor.messages;

/**
 * Message that the DocumentActor sends to himself to recursively count each word in the document.
 * The index is used to know where the current word to compute is in the document.
 */
public final class ComputeWordMessage {

    private final int index;

    public ComputeWordMessage(final int index) {
        this.index = index;
    }

    public int getIndex() {
        return index;
    }
}
