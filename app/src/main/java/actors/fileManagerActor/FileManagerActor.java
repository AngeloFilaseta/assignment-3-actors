package actors.fileManagerActor;

import actors.ActorsName;
import actors.ActorsUtil;
import actors.documentActor.DocumentActor;
import actors.documentActor.messages.BeginComputationMessage;
import actors.fileManagerActor.messages.FinishedComputationMessage;
import actors.fileManagerActor.messages.StartMessage;
import actors.fileManagerActor.messages.StopComputationMessage;
import actors.guiManagerActor.messages.ComputationOverMessage;
import actors.guiManagerActor.messages.PrintMessage;
import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.ActorSelection;
import akka.actor.Props;
import com.codepoetics.protonpack.Indexed;
import com.codepoetics.protonpack.StreamUtils;
import model.Chrono;
import model.DocumentsFolder;
import model.IgnoredWords;
import model.orderedMap.OrderedMap;
import model.orderedMap.OrderedMapComparators;
import model.orderedMap.WordCounter;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public final class FileManagerActor extends AbstractActor {

    private OrderedMap<String, Integer> wordCounter;
    private final Chrono chrono;
    private int remainingActorResults;
    private int nMostFrequentWords;

    private FileManagerActor(){
        chrono = new Chrono();
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(StartMessage.class, this::startComputation)
                .match(FinishedComputationMessage.class, this::finishComputation)
                .match(StopComputationMessage.class, this::stopComputation)
                .build();
    }

    private void startComputation(final StartMessage msg) throws IOException {
        //on start computation message
        wordCounter = WordCounter.createInstance();
        nMostFrequentWords = msg.getArgsContainer().getNMostFrequentWords();
        String documentPath = msg.getArgsContainer().getDocumentPath();
        DocumentsFolder documentsFolder = DocumentsFolder.createInstance(documentPath);
        remainingActorResults = documentsFolder.getPDFDocumentsName().size();
        IgnoredWords ignoredWords = IgnoredWords.createInstance(msg.getArgsContainer().getIgnoredWordsFileName());
        List<Indexed<String>> indexedFileNames = StreamUtils.zipWithIndex(documentsFolder.getPDFDocumentsName().stream())
                .collect(Collectors.toList());
        chrono.start();

        //create a document actor for every document, providing the document path
        indexedFileNames.forEach(indexedFileName -> {
            ActorRef documentActorRef = getContext().actorOf(
                    Props.create(
                            DocumentActor.class,
                            ignoredWords,
                            documentPath + indexedFileName.getValue()
                    ),
                    "file-" + indexedFileName.getIndex()
            );
            //telling the created document actor to start the document computation
            documentActorRef.tell(new BeginComputationMessage(), getSelf());
        });
    }

    private void finishComputation(final FinishedComputationMessage msg) {
        //on computation finished by ANY of the child document actors
        chrono.stop();
        ActorSelection guiActorSelection = getGuiActorSelection();
        wordCounter.putAll(msg.getLocalWordsCount());
        //decreasing the counter of actors that still haven't completed the computation
        remainingActorResults--;
        String message = wordCounter.getOrderedEntry(OrderedMapComparators.orderByMaxToMixByValueComparator(),  nMostFrequentWords).toString();

        //when no actors that have to complete the computation remains, it means that the whole computation is over
        if (remainingActorResults == 0) {
            message = "Computation ended\n" + message + "\nNumber of total words computed:"
                    + wordCounter.getUnorderedMap()
                    .values()
                    .stream()
                    .mapToInt(v -> v)
                    .sum();
            //sending computation over message to gui actor
            guiActorSelection.tell(new ComputationOverMessage(message + "\nTime elapsed: " + chrono.getTime() + "ms"), getSelf());
        } else {
            //sending print message to gui actor to print the updated situation
            guiActorSelection.tell(new PrintMessage(message), getSelf());
        }
    }

    private void stopComputation(final StopComputationMessage msg) {
        //on forceful computation request
        chrono.stop();
        ActorSelection guiActorSelection = getGuiActorSelection();
        //forcefully stoping all document actors
        getContext().getChildren().forEach(c -> getContext().stop(c));
        //sending gui manager actor updates
        guiActorSelection.tell(new PrintMessage("Computation Stopped\nTime elapsed: " + chrono.getTime() + "ms"), getSelf());
    }

    private ActorSelection getGuiActorSelection() {
        return getContext().actorSelection(ActorsUtil.getActorsPath() + ActorsName.MAIN + "/" + ActorsName.GUI);
    }

}
