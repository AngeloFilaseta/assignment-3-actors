package actors.fileManagerActor.messages;

import java.util.Map;

public final class FinishedComputationMessage {

    private final Map<String, Integer> localWordsCount;

    public FinishedComputationMessage(final Map<String, Integer> localWordsCount) {
        this.localWordsCount = localWordsCount;
    }

    public Map<String, Integer> getLocalWordsCount() {
        return localWordsCount;
    }

}
