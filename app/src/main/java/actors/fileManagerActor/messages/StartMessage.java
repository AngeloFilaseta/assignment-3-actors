package actors.fileManagerActor.messages;

import model.ArgsContainer;


public final class StartMessage {

    private final ArgsContainer argsContainer;

    public StartMessage(final ArgsContainer argsContainer) {
        this.argsContainer = argsContainer;
    }

    public ArgsContainer getArgsContainer() {
        return argsContainer;
    }
}
