package actors.guiManagerActor;

import actors.guiManagerActor.messages.ComputationOverMessage;
import actors.guiManagerActor.messages.PrintMessage;
import akka.actor.AbstractActor;
import view.GUI;

public final class GUIManagerActor extends AbstractActor {

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(PrintMessage.class, msg -> {
                    //on print message print the body contained in said message
                    GUI.printOnTextPane(msg.getBody());
                })
                .match(ComputationOverMessage.class, msg -> {
                    //on computation over message print the body contained in said message
                    GUI.printOnTextPane(msg.getBody());
                    //and initialize again the user interface
                    GUI.initializeViewState();
                }).build();
    }

}
