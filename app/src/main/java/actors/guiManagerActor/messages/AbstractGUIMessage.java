package actors.guiManagerActor.messages;

public class AbstractGUIMessage {

    private final String body;

    public AbstractGUIMessage(final String body) {
        this.body = body;
    }

    public final String getBody() {
        return body;
    }

}
