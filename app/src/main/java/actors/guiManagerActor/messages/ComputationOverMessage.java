package actors.guiManagerActor.messages;

/**
 * Message received by GUIManagerActor when the computation is over.
 */
public final class ComputationOverMessage extends AbstractGUIMessage {

    public ComputationOverMessage(final String body) {
        super(body);
    }

}
