package actors.guiManagerActor.messages;

/**
 * Message received by GUIManagerActor to print something in the UI.
 */
public final class PrintMessage extends AbstractGUIMessage {

    public PrintMessage(final String body) {
        super(body);
    }

}
