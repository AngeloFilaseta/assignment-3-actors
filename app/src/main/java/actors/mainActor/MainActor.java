package actors.mainActor;

import actors.ActorsName;
import actors.fileManagerActor.FileManagerActor;
import actors.fileManagerActor.messages.StartMessage;
import actors.guiManagerActor.GUIManagerActor;
import actors.guiManagerActor.messages.PrintMessage;
import actors.mainActor.messages.StartComputationMessage;
import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;

public final class MainActor extends AbstractActor {

    private final ActorRef guiManagerRef;
    private final ActorRef fileManagerRef;

    private MainActor() {
        //initialize gui manager actor and file manager actor
        guiManagerRef = getContext().actorOf(Props.create(GUIManagerActor.class), ActorsName.GUI.toString());
        fileManagerRef = getContext().actorOf(Props.create(FileManagerActor.class), ActorsName.FILE.toString());
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder().match(StartComputationMessage.class, msg -> {
                    //on start computation message tell gui manager actor to adapt gui
                    guiManagerRef.tell(new PrintMessage("Computation Started"), getSelf());
                    //and file manager actor to start computation
                    fileManagerRef.tell(new StartMessage(msg.getArgsContainer()), getSelf());
                }).build();
    }

}
