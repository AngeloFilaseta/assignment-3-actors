package actors.mainActor.messages;

import model.ArgsContainer;

/**
 * Message received by MainActor to actually start the computation.
 */
public final class StartComputationMessage {

    private final ArgsContainer argsContainer;

    public StartComputationMessage(final ArgsContainer argsContainer) {
        this.argsContainer = argsContainer;
    }

    public ArgsContainer getArgsContainer() {
        return this.argsContainer;
    }
}
