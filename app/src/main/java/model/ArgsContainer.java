package model;

public final class ArgsContainer {

    private static final int N_ARGS = 3;

    private final int n;
    private final String f;
    private final String d;

    private ArgsContainer(
            final int nMostFrequentWords,
            final String documentPath,
            final String ignoredWordsFileName) {
        n = nMostFrequentWords;
        f = ignoredWordsFileName;
        d = documentPath;
    }

    public static ArgsContainer createInstance(final String[] args) {
        if (args.length == N_ARGS) {
            int n = Integer.parseInt(args[0]);
            if (args[1] == null || args[2] == null) {
                throw new IllegalStateException();
            }
            return new ArgsContainer(n, args[2], args[1]);
        } else {
            throw new IllegalStateException();
        }
    }

    public int getNMostFrequentWords() {
        return n;
    }

    public String getDocumentPath() {
        return d;
    }

    public String getIgnoredWordsFileName() {
        return f;
    }

}
