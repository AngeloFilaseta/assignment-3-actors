package model;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public final class DocumentContent {

    private static final String DELIMITER = "\\s+";
    private final String pageContent;

    private DocumentContent(final String content) {
        this.pageContent = content;
    }

    public static DocumentContent createInstance(final String content) {
        return new DocumentContent(content);
    }

    public List<String> getWords() {
        return split(removePunctuation(pageContent.toLowerCase()));
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DocumentContent documentContent = (DocumentContent) o;
        return Objects.equals(pageContent, documentContent.pageContent);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pageContent);
    }


    private String removePunctuation(final String string) {
        return string.replaceAll("\\p{Punct}", "");
    }

    private List<String> split(final String string) {
        return Arrays.asList(string.split(DELIMITER));
    }
}
