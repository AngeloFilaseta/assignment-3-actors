package model;

import java.io.File;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public final class DocumentsFolder {

    private final Set<String> documentsName;
    private static final String VALID_EXTENSION = ".pdf";

    private DocumentsFolder(final String folderPath) {
        String[] fileNames = new File(folderPath).list((file, s) -> s.endsWith(VALID_EXTENSION));
        this.documentsName = new HashSet<>();
        if (fileNames != null) {
            this.documentsName.addAll(Arrays.asList(fileNames));
        }
    }

    public static DocumentsFolder createInstance(final String folderPath) {
        return new DocumentsFolder(folderPath);
    }

    public Set<String> getPDFDocumentsName() {
        return documentsName;
    }

}
