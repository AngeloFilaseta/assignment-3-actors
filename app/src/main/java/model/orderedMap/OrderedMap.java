package model.orderedMap;

import java.util.Comparator;
import java.util.List;
import java.util.Map;

public interface OrderedMap<K, V> {

    void put(K key);

    void put(K key, V v);

    void putAll(Map<K, V> map);

    void putAll(OrderedMap<K, V> map);

    V get(K key);

    V remove(K key);

    OrderedMap<K, V> removeAll();

    List<Map.Entry<K, V>> getOrderedEntry(Comparator<Map.Entry<K, V>> comparator);

    List<Map.Entry<K, V>> getOrderedEntry(Comparator<Map.Entry<K, V>> comparator, int limit);

    Map<K, V> getUnorderedMap();

}
