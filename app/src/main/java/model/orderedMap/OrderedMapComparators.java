package model.orderedMap;

import java.util.Comparator;
import java.util.Map.Entry;

public final class OrderedMapComparators {

    private OrderedMapComparators() { }

    public static <K> Comparator<Entry<K, Integer>> orderByMaxToMixByValueComparator() {
        return (e1, e2) -> e2.getValue() - e1.getValue();
    }
}
