package model.orderedMap;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Main data structure, updated by Result when a document has been elaborated,
 * and that saves the completed map at the end of the computation, with all of the words
 * counted.
 */
public final class WordCounter implements OrderedMap<String, Integer> {

    private final Map<String, Integer> localMap;

    private WordCounter() {
        this.localMap = new HashMap<>();
    }

    public static WordCounter createInstance() {
        return new WordCounter();
    }

    private void handlePut(final String key, final Integer value) {
        Integer existingValue = localMap.get(key);
        if (existingValue != null) {
            localMap.put(key, existingValue + value);
        } else {
            this.localMap.put(key, value);
        }
    }

    @Override
    public void put(final String key) {
        handlePut(key, 1);
    }

    @Override
    public void put(final String key, final Integer value) {
        handlePut(key, value);
    }

    @Override
    public void putAll(final Map<String, Integer> map) {
        map.forEach(this::handlePut);
    }

    @Override
    public void putAll(final OrderedMap<String, Integer> map) {
        this.putAll(map.getUnorderedMap());
    }

    @Override
    public Integer get(final String key) {
        return this.localMap.get(key);
    }

    @Override
    public Integer remove(final String key) {
        return this.localMap.remove(key);
    }

    @Override
    public OrderedMap<String, Integer> removeAll() {
        OrderedMap<String, Integer> result = new WordCounter();
        result.putAll(localMap);
        localMap.clear();
        return result;
    }

    @Override
    public List<Entry<String, Integer>> getOrderedEntry(final Comparator<Entry<String, Integer>> comparator) {
        return getOrderedEntry(comparator, this.localMap.size());
    }

    @Override
    public List<Entry<String, Integer>> getOrderedEntry(final Comparator<Entry<String, Integer>> comparator, final int limit) {
        List<Entry<String, Integer>> orderedEntries = new ArrayList<>(localMap.entrySet());
        orderedEntries.sort(comparator);
        if (orderedEntries.size() > limit) {
            orderedEntries = orderedEntries.subList(0, limit);
        }
        return orderedEntries;
    }

    @Override
    public Map<String, Integer> getUnorderedMap() {
        return this.localMap;
    }


}
